<?php
function ubah_huruf($string)
{
	for ($i=0; $i < strlen($string); $i++) { 
	 	$new_str[$i] = chr(ord($string[$i])+1);
	 	}
	 	
	 return implode($new_str)."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
<!-- ord : konversi string ke angka -->
<!-- chr : konversi angka ke string -->
<!-- konversi array ke string -->
